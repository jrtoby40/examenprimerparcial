const mongoose = require("mongoose");
const {Schema}= mongoose;

const AlquilerSchema = new Schema(
    {
   
    Cliente : {type:String},
    Tipo : {type:String},
    jugadores : {type:String},
    costo: {type:String},
    Horas : {type: String},
    Total: {type: String},
    Tipo_de_error: {type: String}
    },

    {
        timestamps:{ createdAt: true, updatedAt:true}
    }
    
    
);



module.exports = mongoose.model("Alquiler", AlquilerSchema);