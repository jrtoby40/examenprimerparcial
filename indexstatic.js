const express = require('express')
const app = express()
const port = 3000
const fs = require('fs');


const home = fs.readFileSync('./view/index.html')

app.get('/', (req, res) => {
  res.write(home)
})

app.listen(port, () => {
  console.log(`Example ${port}`)
})